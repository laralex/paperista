There should be some automations of development process

E.g.:
- scripts for unit-testing subsystems (or all subsystems at once)
- scripts to build subsystems (where applicable)
- scripts to deploy subsystems (where applicable)

If we'll have enough will and courage we can try automatic 
testing/building/deployment not locally by ourselves, 
but on some service connected to GitLab repository, though
I'm sure it costs some money to have such features

Maybe there we can put personal folders for each developer,
say a special folder for me (e.g. alexey_larionov/ where I would
put some thoughts, drafts etc before they reach any working code)